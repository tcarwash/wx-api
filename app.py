#!flask/bin/python
from flask import Flask, flash, jsonify, render_template, Markup, request, abort
import json
from wxstation.trend import getTrend
import templates
from flask_bootstrap import Bootstrap
from time import time, sleep
import pygal
from functools import wraps
from os import environ, path, remove
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, PasswordField
import logging

app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = environ['SECRET_KEY']
Bootstrap(app)

dataLock = "./wxstation/data/data.lock"
aprsLock = "./wxstation/data/aprs.pid"
logfile = "./wxstation/data/logs/wx.log"

"""Logging"""
logger = logging.getLogger("WEB")
logger.setLevel(logging.DEBUG)

fileHandler = logging.FileHandler(logfile)
fileHandler.setLevel(logging.DEBUG)

streamHandler = logging.StreamHandler()
streamHandler.setLevel(logging.WARNING)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fileHandler.setFormatter(formatter)
streamHandler.setFormatter(formatter)

logger.addHandler(fileHandler)
logger.addHandler(streamHandler)

class ObservationForm(Form):
    key = PasswordField('Key:', validators=[validators.required()])
    obs = TextField('Observation:', validators=[validators.required(), validators.Length(min=1, max=20)])

def checkLock():
    if path.isfile(dataLock):
        logger.debug('File is in use')
        return True

def getLock():
    while checkLock():
        pass
    open(dataLock, 'a').close()
    logger.debug('Lock acquired')
    return True

def releaseLock():
    logger.debug('Lock released')
    remove(dataLock)

def aprsActive():
    if path.isfile(aprsLock):
        return True
    else:
        return False

@app.route("/form/", methods=['GET', 'POST'])
def observation():
    form = ObservationForm(request.form)
    print(form.errors)
    if request.method == 'POST':
        key=request.form['key']
        obs=request.form['obs']
        if form.validate():
            # Save the comment here.
            if key == environ['APPKEY']:
                data = []
                if getLock():
                    with open("./wxstation/data/data.json", 'r') as f:
                        data = json.load(f)
                    data[-1]['observation'] = obs
                    with open("./wxstation/data/data.json", 'w') as f:
                        json.dump(data, f)
                    flash("\"" + data[-1]['observation'] + "\"" + " - was added as an observation" )
                    logger.info('Weather observation added via webpage')
                    releaseLock()
            else:
                flash("Error: Key was incorrect")
        else:
            flash('Error: All the form fields are required. And max observation length is 20 characters')
    return render_template('obsForm.html', form=form)

def require_appkey(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        if request.args.get('key') and request.args.get('key') == environ['APPKEY']:
            return view_function(*args, **kwargs)
        else:
            abort(401)
    return decorated_function

@app.route('/', methods=['GET'])
def index():
    while checkLock():
        pass
    with open("./wxstation/data/data.json", 'r') as f:
        data = json.load(f)
    timestamp = []
    pres = []
    hum = []
    for item in data:
            pres.append(item['barometer'])
            timestamp.append(item['timestamp'])
            hum.append(item['humidity'])
    line_chart = pygal.Line(show_only_major_dots=True)
    line_chart.x_labels = timestamp[-1000:]
    line_chart.add('Humidity(%)', hum[-1000:])
    line_chart.add('Pressure(mbar)', pres[-1000:], secondary=True)
    graph_data = line_chart.render_data_uri()
    try:
        trend=getTrend(data)
    except:
        trend=Markup('<span class="label label-danger">Can\'t determine trend</span>')
    print(trend)
    return render_template("index.html", title="Home", data=data, trend=trend, timenow=time(), graph_data=graph_data, aprs=aprsActive())

@app.route('/wx/data', methods=['GET'])
def get_data():
    while checkLock():
        sleep(.5)
    data = []
    with open("./wxstation/data/data.json", 'r') as f:
        data = json.load(f)
    return jsonify(data)

@app.route('/aprs', methods=['GET'])
def get_aprs():
    if aprsActive():
        response = jsonify(success=True)
    else:
        response = jsonify(success=False)
    return response

@app.route('/usage', methods=['GET'])
def api_usage():
    return render_template("usage.html", title="API Usage")

@app.route('/about', methods=['GET'])
def about():
    return render_template("about.html", title="About the Station")


@app.route('/wx/data/latest', methods=['GET'])
def get_latest():
    while checkLock():
        sleep(1)
    data = []
    with open("./wxstation/data/data.json", 'r') as f:
        data = json.load(f)
    data[-1]["bar-trend"] = getTrend(data)
    return jsonify(data[-1])

@app.route('/observation/', methods=['POST'])
@require_appkey
def post_obs(*args, **kwargs):
    if request.args.get('obs'):
        if getLock():
            data = []
            with open("./wxstation/data/data.json", 'r') as f:
                data = json.load(f)
            data[-1]['observation'] = request.args.get('obs')
            with open("./wxstation/data/data.json", 'w') as f:
                json.dump(data, f)
            logger.info('Observation added via api')
            releaseLock()
    else:
        abort(500)
    return jsonify(data[-1])

if __name__ == '__main__':

    app.run(host="0.0.0.0", debug=False)
